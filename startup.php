<?php

function startup()
{
	// Настройки подключения к БД.
	$hostname = 'localhost'; 
	$username = 'root'; 
	$password = '';
	$dbName = 'lesson1';
	
	// Языковая настройка.
	setlocale(LC_ALL, 'ru_RU.UTF8');	//ru_RU.UTF8
	
	// Подключение к БД.
	mysql_connect($hostname, $username, $password) or die('No connect with database'); 
	mysql_query('SET NAMES utf8'); //utf8
	mysql_select_db($dbName) or die('No database');

	// Открытие сессии.
	session_start();
		
}
